import Link from "next/link";
import NavBar from "../components/NavBar";

export default function IndexPage() {
  return (
    <div>
      <NavBar />
      <div>
        <img
          style={{ width: 250 }}
          src="https://i.ytimg.com/vi/rLqH4K5u22k/hqdefault.jpg"
          alt="homepage"
        />
      </div>

      <div className="font-bold flex hover:text-green-800">
        Hello World.{getName()}, today is: {myTest(firstName)}
        <div>
          <Link href="/about">
            <a className="text-pupple-700" href="##">
              About
            </a>
          </Link>
        </div>
        <div>{moreTest(firstName)}</div>
      </div>
    </div>
  );
}

const firstName = "Steve";

function getName() {
  return "Hey, Tom!";
}

function myTest() {
  var myDate = new Date();
  //return myDate.toString;
  return "10/02/2020 Today";
}

function moreTest(firstn) {
  return (
    <div>
      <div>More Test, {firstn}</div>
    </div>
  );
  console.log(firstn);
}
